package tf.bug.fancadetagless

import tf.bug.fancadegraph.BlockDefinition

class FancadeSuite extends munit.FunSuite {
  
  test("A capture creates one block") {
    val capture: FancadeF = Fix(Fancade.Capture[String](BlockDefinition.GetNumber, "hello", Vector(
      BlockDefinition.GetNumber.output
    )))
    assertEquals(Fancade.render(capture).blocks.size, 1)
  }

  test("Stacking 1 + 1 results in a 2-element map") {
    val one: FancadeF = Fix(Fancade.Capture(BlockDefinition.NumberValue, 1.0f, Vector(
      BlockDefinition.NumberValue.output
    )))
    val add: FancadeF = Fix(Fancade.Use(
      Vector(one, one),
      BlockDefinition.AddNumbers,
      (),
      Vector(
        BlockDefinition.AddNumbers.input1,
        BlockDefinition.AddNumbers.input2
      ),
      Vector(
        BlockDefinition.AddNumbers.output
      )
    ))
    val stacked = Fancade.stack(add)
    assertEquals(stacked, Map(
      1 -> Fancade.Capture(BlockDefinition.NumberValue, 1.0f, Vector(BlockDefinition.NumberValue.output)),
      0 -> Fancade.Use(Vector(1, 1), BlockDefinition.AddNumbers, (),
        Vector(
          BlockDefinition.AddNumbers.input1,
          BlockDefinition.AddNumbers.input2
        ),
        Vector(BlockDefinition.AddNumbers.output)
      )
    ))
  }

  test("A constant creates a capture") {
    val program = new Fanscript.Program[Float] {
      override def run[F[_]](implicit interp: Fanscript[F]): F[Float] = interp.lift(1.0f)
    }
    val result = program.run[FancadeW]
    assertEquals(result, Fancade.Capture(
      BlockDefinition.NumberValue,
      1.0f,
      Vector(BlockDefinition.NumberValue.output)
    ))
  }

}
