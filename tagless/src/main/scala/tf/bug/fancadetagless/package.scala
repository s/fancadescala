package tf.bug

import higherkindness.droste.data.Fix

package object fancadetagless {
  
    type Fancade[A] = Fix[FancadeF]

}
