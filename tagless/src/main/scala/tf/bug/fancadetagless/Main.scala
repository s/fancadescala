package tf.bug.fancadetagless

import cats.effect.{Blocker, ContextShift, ExitCode, IO, IOApp, Sync}
import fs2._
import higherkindness.droste.data._
import java.nio.file.{Paths, StandardOpenOption}
import scodec.stream.StreamEncoder
import tf.bug.fancadegraph.Level
import tf.bug.fancadescodec.Cartridge
import tf.bug.fancadescodec.Codecs

object Main extends IOApp {
  
  override def run(args: List[String]): IO[ExitCode] =
    program[IO].compile.drain.as(ExitCode.Success)
  
  def program[F[_] : Sync : ContextShift]: Stream[F, Unit] = {
    val program = new Fanscript.Program[Unit] {
      override def run[G[_]](implicit interp: Fanscript[G]): G[Unit] = {
        val score = interp.numberVariable("Score")
        val one = interp.lift(1.0F)
        val addToScore = interp.addNumbers(score, one)
        val setScore = interp.setNumber(score, addToScore, () => interp.unit)
        val inspectScore = interp.inspect(score, () => setScore)
        inspectScore
      }
    }

    val fancade: Fix[FancadeF] = program.run[Fancade]

    val level: Level = FancadeF.render(fancade)

    val cart: Cartridge = Cartridge(
      "Test",
      "rayc",
      "Test of tagless",
      Vector(
        Level.encode(level)
      )
    )

    val output = Paths.get("""C:\Users\aprim\Documents\FancadeLevels\TaglessTest""")

    (for {
      blocker <- Stream.resource(Blocker[F])
      result <- Stream.emit[F, Cartridge](cart)
          .through(StreamEncoder.once(Codecs.encCartridge).toPipeByte[F])
          .through(io.file.writeAll(output, blocker, Seq(StandardOpenOption.TRUNCATE_EXISTING)))
    } yield result)
  }

}
