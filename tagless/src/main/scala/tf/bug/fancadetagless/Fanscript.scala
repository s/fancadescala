package tf.bug.fancadetagless

import higherkindness.droste.data._
import polymorphic._
import tf.bug.fancadetagless.Fanscript.{Position, ScreenSize}
import tf.bug.fancadescodec.Metadata.Sample
import tf.bug.fancadegraph.BlockDefinition
import tf.bug.fancadegraph.Template

trait Fanscript[F[_]] {

  def unit: F[Unit]

  def lift(value: Float): F[Float]
  def lift(value: Vector3): F[Vector3]
  def lift(value: Rotation): F[Rotation]
  def lift(value: Boolean): F[Boolean]

  def inspect(value: F[Float], after: () => F[Unit]): F[Unit]

  def win(stop: Boolean)(after: () => F[Unit]): F[Unit]
  def lose(stop: Boolean)(after: () => F[Unit]): F[Unit]
  def setScore(score: F[Float], after: () => F[Unit]): F[Unit]
  def setCamera(position: F[Vector3], rotation: F[Rotation], distance: F[Float], after: () => F[Unit]): F[Unit]
  def setLight(position: F[Vector3], rotation: F[Rotation], after: () => F[Unit]): F[Unit]
  def screenSize: F[Fanscript.ScreenSize]
  def accelerometer: F[Vector3]

  def getPosition(obj: F[Obj]): F[Fanscript.Position]
  def setPosition(obj: F[Obj], position: F[Vector3], rotation: F[Rotation], after: () => F[Unit]): F[Unit]
  def raycast(from: F[Vector3], to: F[Vector3]): F[Fanscript.Raycast]
  def getSize(obj: F[Obj]): F[Fanscript.Size]
  def setVisible(obj: F[Obj], visible: F[Boolean], after: () => F[Unit]): F[Unit]
  def createObj(original: F[Obj], after: F[Obj] => F[Unit]): F[Unit]
  def destroyObj(obj: F[Obj], after: () => F[Unit]): F[Unit]

  def playSound(loop: Boolean, sound: Sample)(volume: F[Float], pitch: F[Float], after: F[Float] => F[Unit]): F[Unit]
  def stopSound(channel: F[Float], after: () => F[Unit]): F[Unit]
  def volumePitch(channel: F[Float], volume: F[Float], pitch: F[Float], after: () => F[Unit]): F[Unit]

  def addForce(obj: F[Obj], force: F[Vector3], applyAt: F[Vector3], torque: F[Vector3], after: () => F[Unit]): F[Unit]
  def getVelocity(obj: F[Obj]): F[Fanscript.Velocity]
  def setVelocity(obj: F[Obj], velocity: F[Vector3], spin: F[Vector3], after: () => F[Unit]): F[Unit]
  def setLocked(obj: F[Obj], position: F[Vector3], rotation: F[Vector3], after: () => F[Unit]): F[Unit]
  def setMass(obj: F[Obj], mass: F[Float], after: () => F[Unit]): F[Unit]
  def setFriction(obj: F[Obj], friction: F[Float]): F[Unit]
  def setBounciness(obj: F[Obj], bounciness: F[Float]): F[Unit]
  def setGravity(gravity: F[Vector3]): F[Unit]
  def addConstraint(base: F[Obj], part: F[Obj], pivot: F[Vector3]): F[Constraint]
  def linearLimits(constraint: F[Constraint], lower: F[Vector3], upper: F[Vector3]): F[Unit]
  def angularLimits(constraint: F[Constraint], lower: F[Vector3], upper: F[Vector3]): F[Unit]
  def linearSpring(constraint: F[Constraint], stiffness: F[Vector3], damping: F[Vector3]): F[Unit]
  def angularSpring(constraint: F[Constraint], stiffness: F[Vector3], damping: F[Vector3]): F[Unit]
  def linearMotor(constraint: F[Constraint], speed: F[Vector3], force: F[Vector3]): F[Unit]
  def angularMotor(constraint: F[Constraint], speed: F[Vector3], force: F[Vector3]): F[Unit]

  def ifElse(cond: F[Boolean])(ifTrue: F[Unit])(ifFalse: F[Unit]): F[Unit]
  def onPlay(onPlay: F[Unit]): F[Unit]
  def onBoxArt(onBoxArt: F[Unit]): F[Unit]
  def onTouch(onTouch: F[Fanscript.Touch] => F[Unit]): F[Unit]

  def addNumbers(a: F[Float], b: F[Float]): F[Float]

  def numberVariable(name: String): F[Float]
  def setNumber(reference: F[Float], value: F[Float], after: () => F[Unit]): F[Unit]

  def screenSizeWidth(screenSize: F[ScreenSize]): F[Float]
  def screenSizeHeight(screenSize: F[ScreenSize]): F[Float]

  def positionPosition(position: F[Position]): F[Vector3]
  def positionRotation(position: F[Position]): F[Rotation]

  def raycastHit(raycast: F[Fanscript.Raycast]): F[Boolean]
  def raycastPosition(raycast: F[Fanscript.Raycast]): F[Vector3]
  def raycastObj(raycast: F[Fanscript.Raycast]): F[Obj]

  def sizeMin(size: F[Fanscript.Size]): F[Vector3]
  def sizeMax(size: F[Fanscript.Size]): F[Vector3]

  def velocityVelocity(velocity: F[Fanscript.Velocity]): F[Vector3]
  def velocitySpin(velocity: F[Fanscript.Velocity]): F[Vector3]

}

object Fanscript {

  trait Program[R] {
    def run[F[_]](implicit interp: Fanscript[F]): F[R]
  }

  implicit object fancade extends Fanscript[Fancade] {

    override def unit: Fancade[Unit] =
      Fix(FancadeF.Noop())

    override def lift(value: Float): Fancade[Float] =
      Fix(FancadeF.Create(
        Vector(),
        Exists(Template(
          BlockDefinition.NumberValue,
          value
        )),
        Vector(),
        Vector(BlockDefinition.NumberValue.output)
      ))
    override def lift(value: Vector3): Fancade[Vector3] = ???
    override def lift(value: Rotation): Fancade[Rotation] = ???
    override def lift(value: Boolean): Fancade[Boolean] = ???

    override def inspect(value: Fancade[Float], after: () => Fancade[Unit]): Fancade[Unit] = {
      val us = Fix(FancadeF.Create(
        Vector(value),
        Exists(Template(
          BlockDefinition.InspectNumber,
          ()
        )),
        Vector(BlockDefinition.InspectNumber.value),
        Vector(BlockDefinition.InspectNumber.before)
      ))
      Fix(FancadeF.Sequence(
        us,
        BlockDefinition.InspectNumber.after,
        after()
      ))
    }
    
    override def win(stop: Boolean)(after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def lose(stop: Boolean)(after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def setScore(score: Fancade[Float], after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def setCamera(position: Fancade[Vector3], rotation: Fancade[Rotation], distance: Fancade[Float], after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def setLight(position: Fancade[Vector3], rotation: Fancade[Rotation], after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def screenSize: Fancade[ScreenSize] =
      Fix(FancadeF.Create(
        Vector(),
        Exists(Template(
          BlockDefinition.ScreenSize,
          ()
        )),
        Vector(),
        Vector(BlockDefinition.ScreenSize.screenWidth, BlockDefinition.ScreenSize.screenHeight),
      ))
    override def accelerometer: Fancade[Vector3] = ???
    
    override def getPosition(obj: Fancade[Obj]): Fancade[Position] =
      Fix(FancadeF.Create(
        Vector(obj),
        Exists(Template(
          BlockDefinition.GetPosition,
          ()
        )),
        Vector(BlockDefinition.GetPosition.obj),
        Vector(BlockDefinition.GetPosition.position)
      ))
    override def setPosition(obj: Fancade[Obj], position: Fancade[Vector3], rotation: Fancade[Rotation], after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def raycast(from: Fancade[Vector3], to: Fancade[Vector3]): Fancade[Raycast] = ???
    override def getSize(obj: Fancade[Obj]): Fancade[Size] = ???
    override def setVisible(obj: Fancade[Obj], visible: Fancade[Boolean], after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def createObj(original: Fancade[Obj], after: (Fancade[Obj]) => Fancade[Unit]): Fancade[Unit] = ???
    override def destroyObj(obj: Fancade[Obj], after: () => Fancade[Unit]): Fancade[Unit] = ???
    
    override def playSound(loop: Boolean, sound: Sample)(volume: Fancade[Float], pitch: Fancade[Float], after: Fancade[Float] => Fancade[Unit]): Fancade[Unit] = ???
    override def stopSound(channel: Fancade[Float], after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def volumePitch(channel: Fancade[Float], volume: Fancade[Float], pitch: Fancade[Float], after: () => Fancade[Unit]): Fancade[Unit] = ???
    
    override def addForce(obj: Fancade[Obj], force: Fancade[Vector3], applyAt: Fancade[Vector3], torque: Fancade[Vector3], after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def getVelocity(obj: Fancade[Obj]): Fancade[Fanscript.Velocity] = ???
    override def setVelocity(obj: Fancade[Obj], velocity: Fancade[Vector3], spin: Fancade[Vector3], after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def setLocked(obj: Fancade[Obj], position: Fancade[Vector3], rotation: Fancade[Vector3], after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def setMass(obj: Fancade[Obj], mass: Fancade[Float], after: () => Fancade[Unit]): Fancade[Unit] = ???
    override def setFriction(obj: Fancade[Obj], friction: Fancade[Float]): Fancade[Unit] = ???
    override def setBounciness(obj: Fancade[Obj], bounciness: Fancade[Float]): Fancade[Unit] = ???
    override def setGravity(gravity: Fancade[Vector3]): Fancade[Unit] = ???
    override def addConstraint(base: Fancade[Obj], part: Fancade[Obj], pivot: Fancade[Vector3]): Fancade[Constraint] = ???
    override def linearLimits(constraint: Fancade[Constraint], lower: Fancade[Vector3], upper: Fancade[Vector3]): Fancade[Unit] = ???
    override def angularLimits(constraint: Fancade[Constraint], lower: Fancade[Vector3], upper: Fancade[Vector3]): Fancade[Unit] = ???
    override def linearSpring(constraint: Fancade[Constraint], stiffness: Fancade[Vector3], damping: Fancade[Vector3]): Fancade[Unit] = ???
    override def angularSpring(constraint: Fancade[Constraint], stiffness: Fancade[Vector3], damping: Fancade[Vector3]): Fancade[Unit] = ???
    override def linearMotor(constraint: Fancade[Constraint], speed: Fancade[Vector3], force: Fancade[Vector3]): Fancade[Unit] = ???
    override def angularMotor(constraint: Fancade[Constraint], speed: Fancade[Vector3], force: Fancade[Vector3]): Fancade[Unit] = ???
    
    override def ifElse(cond: Fancade[Boolean])(ifTrue: Fancade[Unit])(ifFalse: Fancade[Unit]): Fancade[Unit] = ???
    override def onPlay(onPlay: Fancade[Unit]): Fancade[Unit] = ???
    override def onBoxArt(onBoxArt: Fancade[Unit]): Fancade[Unit] = ???
    override def onTouch(onTouch: Fancade[Touch] => Fancade[Unit]): Fancade[Unit] = ???

    override def addNumbers(a: Fancade[Float], b: Fancade[Float]): Fancade[Float] =
      Fix(FancadeF.Create(
        Vector(a, b),
        Exists(Template(
          BlockDefinition.AddNumbers,
          ()
        )),
        Vector(BlockDefinition.AddNumbers.input1, BlockDefinition.AddNumbers.input2),
        Vector(BlockDefinition.AddNumbers.output)
      ))
  
    override def screenSizeWidth(screenSize: Fancade[ScreenSize]): Fancade[Float] =
      Fix(FancadeF.Select(
        screenSize,
        Vector(0)
      ))
    override def screenSizeHeight(screenSize: Fancade[ScreenSize]): Fancade[Float] =
      Fix(FancadeF.Select(
        screenSize,
        Vector(1)
      ))

    override def numberVariable(name: String): Fancade[Float] =
      Fix(FancadeF.Create(
        Vector(),
        Exists(Template(
          BlockDefinition.GetNumber,
          name
        )),
        Vector(),
        Vector(BlockDefinition.GetNumber.output)
      ))
    override def setNumber(reference: Fancade[Float], value: Fancade[Float], after: () => Fancade[Unit]): Fancade[Unit] = {
      val us = Fix(FancadeF.Create(
        Vector(reference, value),
        Exists(Template(
          BlockDefinition.SetNumberReference,
          ()
        )),
        Vector(BlockDefinition.SetNumberReference.reference, BlockDefinition.SetNumberReference.value),
        Vector(BlockDefinition.SetNumberReference.before)
      ))
      Fix(FancadeF.Sequence(
        us,
        BlockDefinition.SetNumberReference.after,
        after()
      ))
    }
    
    override def positionPosition(position: Fancade[Position]): Fancade[Vector3] = ???
    override def positionRotation(position: Fancade[Position]): Fancade[Rotation] = ???
    
    override def raycastHit(raycast: Fancade[Raycast]): Fancade[Boolean] = ???
    override def raycastPosition(raycast: Fancade[Raycast]): Fancade[Vector3] = ???
    override def raycastObj(raycast: Fancade[Raycast]): Fancade[Obj] = ???
    
    override def sizeMin(size: Fancade[Size]): Fancade[Vector3] = ???
    override def sizeMax(size: Fancade[Size]): Fancade[Vector3] = ???
    
    override def velocityVelocity(velocity: Fancade[Velocity]): Fancade[Vector3] = ???
    override def velocitySpin(velocity: Fancade[Velocity]): Fancade[Vector3] = ???

  }

  case class ScreenSize(width: Float, height: Float)
  case class Position(position: Vector3, rotation: Rotation)
  case class Raycast(hit: Boolean, position: Vector3, obj: Obj)
  case class Size(min: Vector3, max: Vector3)
  case class Velocity(velocity: Vector3, spin: Vector3)
  case class Touch(screenX: Float, screenY: Float)

}
