package tf.bug.fancadetagless

case class Vector3(
  x: Float,
  y: Float,
  z: Float
)
