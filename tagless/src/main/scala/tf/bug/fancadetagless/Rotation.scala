package tf.bug.fancadetagless

case class Rotation(
  pitch: Float,
  yaw: Float,
  roll: Float
)
