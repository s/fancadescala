package tf.bug.fancadescodec

case class Wire(
  startNearLeftBottom: Position,
  endNearLeftBottom: Position,
  startVoxel: Position,
  endVoxel: Position
               )
