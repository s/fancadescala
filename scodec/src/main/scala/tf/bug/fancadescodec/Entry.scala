package tf.bug.fancadescodec

case class Entry(
  visibilityFlags: Option[Int],
  name: String,
  faces: Option[Faces],
  world: Option[World],
  metadatas: Option[Vector[Metadata]],
  wires: Option[Vector[Wire]]
)
