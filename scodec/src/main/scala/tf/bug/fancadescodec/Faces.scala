package tf.bug.fancadescodec

case class Faces(
                  faceRight: Vector[Int],
                  faceLeft: Vector[Int],
                  faceUp: Vector[Int],
                  faceDown: Vector[Int],
                  faceFar: Vector[Int],
                  faceNear: Vector[Int]
                )
