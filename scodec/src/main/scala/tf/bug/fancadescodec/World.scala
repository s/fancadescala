package tf.bug.fancadescodec

case class World(
  boundary: Boundary,
  objs: Vector[Obj]
                )
