package tf.bug.fancadescodec

sealed trait Metadata

object Metadata {

  case class Bool(
    block: Position,
    doLoop: Boolean
  ) extends Metadata

  case class PlaySoundSample(
    block: Position,
    sample: Sample
  ) extends Metadata

  sealed trait Sample
  object Sample {
    case object Chirp extends Sample
    case object Scrape extends Sample
    case object Squeek extends Sample
    case object Engine extends Sample
    case object Button extends Sample
    case object Ball extends Sample
    case object Piano extends Sample
    case object Marimba extends Sample
    case object Pad extends Sample
    case object Beep extends Sample
    case object Plop extends Sample
    case object Flop extends Sample
    case object Splash extends Sample
    case object Boom extends Sample
    case object Hit extends Sample
    case object Clang extends Sample
    case object Jump extends Sample
  }
  
  case class Number(
    block: Position,
    value: Float
  ) extends Metadata

  case class Text(
    block: Position,
    name: String
  ) extends Metadata

  case class Triple(
    block: Position,
    x: Float,
    y: Float,
    z: Float
  ) extends Metadata

  case class Terminal(
    voxel: Position,
    name: String
  ) extends Metadata

}