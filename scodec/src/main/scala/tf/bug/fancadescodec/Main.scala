package tf.bug.fancadescodec

import cats.effect._
import cats.implicits._
import fs2._
import java.nio.file.Paths
import scodec.stream.{StreamDecoder, StreamEncoder}

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    (Stream.resource(Blocker[IO]) >>= read[IO]).compile.drain.as(ExitCode.Success)
  }

  def read[F[_] : RaiseThrowable : Sync : ContextShift](blocker: Blocker): Stream[F, Unit] = {
    val input = Paths.get("""C:\Users\aprim\Documents\FancadeLevels\TaglessTest""")
    io.file.readAll[F](input, blocker, 1024)
      .through(StreamDecoder.once(Codecs.encCartridge).toPipeByte)
      .evalMap(c => Sync[F].delay(println(c)))
  }

  def write[F[_] : RaiseThrowable : Sync : ContextShift](blocker: Blocker): Stream[F, Unit] = {
    val output = Paths.get("""C:\Users\aprim\Documents\FancadeLevels\AllTheSamples""")

    val outputCartridge = Cartridge(
      "AllTheSamples",
      "rayc",
      "A Fancade game",
      Vector(
        Entry(
          Some(0x03),
          "New Level",
          None,
          Some(
            World(
              Boundary(2, 17, 2),
              Vector.fill(17)(Vector(Obj.playSound0, Obj.playSound1)).flatten ++ Vector.fill(17)(Vector(Obj.playSound2, Obj.playSound3)).flatten
            )
          ),
          Some(
            (0 until 17).map { case h =>
              Metadata.PlaySoundSample(
                Position(0, h, 0),
                h match {
                  case 0 => Metadata.Sample.Chirp
                  case 1 => Metadata.Sample.Scrape
                  case 2 => Metadata.Sample.Squeek
                  case 3 => Metadata.Sample.Engine
                  case 4 => Metadata.Sample.Button
                  case 5 => Metadata.Sample.Ball
                  case 6 => Metadata.Sample.Piano
                  case 7 => Metadata.Sample.Marimba
                  case 8 => Metadata.Sample.Pad
                  case 9 => Metadata.Sample.Beep
                  case 10 => Metadata.Sample.Plop
                  case 11 => Metadata.Sample.Flop
                  case 12 => Metadata.Sample.Splash
                  case 13 => Metadata.Sample.Boom
                  case 14 => Metadata.Sample.Hit
                  case 15 => Metadata.Sample.Clang
                  case 16 => Metadata.Sample.Jump
                }
              )
            }.toVector
          ),
          None
        )
      )
    )

    Stream.emit[F, Cartridge](outputCartridge)
      .through(StreamEncoder.once(Codecs.encCartridge).toPipeByte)
      .through(io.file.writeAll[F](output, blocker))
  }

  val paletteMap: ((Int, Int, Int)) => String = Map(
    (238, 165, 162) -> "Brown 1",
    (255, 255, 255) -> "Tan 1",
    (192, 152, 255) -> "Purple 1",
    (255, 203, 252) -> "Pink 1",
    (255, 170, 170) -> "Red 1",
    (255, 185, 142) -> "Orange 1",
    (255, 255, 134) -> "Yellow 1",
    (194, 255, 118) -> "Green 1",
    (0, 205, 255) -> "Blue 1",
    (255, 255, 255) -> "White 1",
    (109, 111, 126) -> "Black 1",
    (203, 117, 137) -> "Brown 2",
    (255, 231, 207) -> "Tan 2",
    (153, 122, 234) -> "Purple 2",
    (255, 164, 224) -> "Pink 2",
    (255, 81, 115) -> "Red 2",
    (255, 123, 91) -> "Orange 2",
    (255, 234, 0) -> "Yellow 2",
    (72, 208, 85) -> "Green 2",
    (0, 154, 255) -> "Blue 2",
    (206, 208, 216) -> "White 2",
    (68, 68, 83) -> "Black 2",
    (156, 90, 100) -> "Brown 3",
    (255, 195, 180) -> "Tan 3",
    (113, 91, 173) -> "Purple 3",
    (255, 123, 186) -> "Pink 3",
    (205, 59, 86) -> "Red 3",
    (215, 89, 71) -> "Orange 3",
    (246, 185, 0) -> "Yellow 3",
    (0, 147, 82) -> "Green 3",
    (0, 117, 237) -> "Blue 3",
    (156, 158, 173) -> "White 3",
    (31, 31, 41) -> "Black 3",
  ).withDefaultValue("Transparent")
  val indexMap: String => Int = List(
    "Black",
    "White",
    "Brown",
    "Tan",
    "Red",
    "Orange",
    "Yellow",
    "Green",
    "Blue",
    "Purple",
    "Pink"
  ).flatMap(n => List(s"$n 3", s"$n 2", s"$n 1")).zipWithIndex.map { case (n, i) => (n, i + 1) }.toMap.withDefaultValue(0)

}
