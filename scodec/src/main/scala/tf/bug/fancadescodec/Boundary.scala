package tf.bug.fancadescodec

case class Position(
  lr: Int,
  du: Int,
  nf: Int
                   )

case class Boundary(
  sizeLR: Int,
  sizeDU: Int,
  sizeNF: Int
                   )
