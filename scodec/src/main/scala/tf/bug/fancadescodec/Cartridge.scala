package tf.bug.fancadescodec

case class Cartridge(
  title: String,
  author: String,
  description: String,
  entries: Vector[Entry]
                    )
