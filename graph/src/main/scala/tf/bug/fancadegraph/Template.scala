package tf.bug.fancadegraph

case class Template[T](definition: BlockDefinition[T], args: T)(implicit val ev: Argument[T])
