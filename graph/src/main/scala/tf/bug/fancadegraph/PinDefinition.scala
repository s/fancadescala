package tf.bug.fancadegraph

import tf.bug.fancadescodec.Position

case class PinDefinition(
  dataType: PinType,
  direction: PinDirection,
  voxel: Position
)

sealed trait PinType
object PinType {

  case object Number extends PinType
  case object Object extends PinType
  case object Vector extends PinType
  case object Rotation extends PinType
  case object Truth extends PinType
  case object Constraint extends PinType
  case object Pull extends PinType

  case class Pointer(of: PinType) extends PinType

}

sealed trait PinDirection
object PinDirection {

  case object Input extends PinDirection
  case object Output extends PinDirection

}

object PinPosition {

  def bottom(n: Int): Position = Position(3 + (8 * n), 1, 0)
  def top(depth: Int)(n: Int): Position = Position(3 + (8 * n), 1, 6 + (8 * (depth - 1)))
  def left(n: Int): Position = Position(0, 1, 3 + (8 * n))
  def right(width: Int)(n: Int): Position = Position(6 + (8 * (width - 1)), 1, 3 + (8 * n))

  val top1: Int => Position = top(1)
  val top2: Int => Position = top(2)
  val top3: Int => Position = top(3)
  val top4: Int => Position = top(4)
  val right2: Int => Position = right(2)

}
