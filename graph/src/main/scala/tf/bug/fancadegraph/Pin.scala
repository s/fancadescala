package tf.bug.fancadegraph

import tf.bug.fancadescodec.Position

case class Pin(
  basePosition: Position,
  definition: PinDefinition
)
