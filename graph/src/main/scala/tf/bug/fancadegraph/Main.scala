package tf.bug.fancadegraph

import cats.effect.{Blocker, ExitCode, IO, IOApp}
import fs2._
import java.nio.file.{Paths, StandardOpenOption}
import polymorphic._
import scalax.collection.Graph
import scalax.collection.GraphEdge.DiHyperEdge
import scodec.stream.StreamEncoder
import tf.bug.fancadescodec.{Cartridge, Codecs, Position}

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    val winBlock = Block(Position(0, 0, 0), Exists(Template(BlockDefinition.Win, true)))
    val loseBlock = Block(Position(0, 0, 3), Exists(Template(BlockDefinition.Lose, true)))
    val loseAfter = Pin(loseBlock.position, BlockDefinition.Lose.after)
    val winBefore = Pin(winBlock.position, BlockDefinition.Win.before)
    val level = Level(
      Set(
        winBlock,
        loseBlock
      ),
      Graph[Pin, DiHyperEdge](
        DiHyperEdge(winBefore, loseAfter)
      )
    )

    val output = Paths.get("""C:\Users\aprim\Documents\FancadeLevels\GraphTest""")
    val cart = Cartridge(
      "Graph Test",
      "Raymond AC",
      "Test of Fancade Graph Library",
      Vector(
        Level.encode(level)
      )
    )

    (for {
      blocker <- Stream.resource(Blocker[IO])
      result <- Stream.emit[IO, Cartridge](cart)
          .through(StreamEncoder.once(Codecs.encCartridge).toPipeByte[IO])
          .through(io.file.writeAll(output, blocker, Seq(StandardOpenOption.CREATE_NEW)))
    } yield result).compile.drain.as(ExitCode.Success)
  }

}
