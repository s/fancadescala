package tf.bug.fancadegraph

import polymorphic._
import tf.bug.fancadescodec.{Metadata, Position}

case class Block(
  position: Position,
  template: Exists[Template]
) {
  def metadata: Vector[Metadata] = {
    val t = Exists.unwrap(template)
    t.ev.encode(t.args)(position)
  }
}
