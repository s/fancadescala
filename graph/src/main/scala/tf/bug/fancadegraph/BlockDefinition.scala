package tf.bug.fancadegraph

import tf.bug.fancadescodec.Obj

sealed trait BlockDefinition[T] {
  val width: Int
  val height: Int
  val depth: Int
  val pins: Vector[PinDefinition]

  val ids: Vector[Obj] // FIXME support custom blocks
}

object BlockDefinition {

  sealed trait BuiltIn[T] extends BlockDefinition[T] {
    val ids: Vector[Obj]
  }

  sealed trait Tangible[T] extends BuiltIn[T] {
    final override val pins: Vector[PinDefinition] = Vector() // FIXME
  }

  case object Bricks extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.bricks)
  }
  case object Grass extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.grass)
  }
  case object GrassDotty extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.grassDotty)
  }
  case object Dirt extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.dirt)
  }
  case object DirtDotty extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.dirtDotty)
  }
  case object DirtSlab extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.dirtSlab)
  }
  case object FoliageSlab extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.foliageSlab)
  }
  case object FoliageDotty extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.foliageDotty)
  }
  case object FoliageTop extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.foliageTop)
  }
  case object Foliage extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.foliage)
  }
  case object FoliageBottom extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.foliageBottom)
  }
  case object Stone extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stone)
  }
  case object StoneDotty extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stoneDotty)
  }
  case object StoneSlab extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stoneSlab)
  }
  case object StoneTop extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stoneTop)
  }
  case object StoneBottom extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stoneBottom)
  }
  case object StoneBlock extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stoneBlock)
  }
  case object StonePillar extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stonePillar)
  }
  case object StoneHalf extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stoneHalf)
  }
  case object Slate extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.slate)
  }
  case object SlateDotty extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.slateDotty)
  }
  case object SlateNE extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.slateNE)
  }
  case object SlateNW extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.slateNW)
  }
  case object SlateSE extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.slateSE)
  }
  case object SlateSW extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.slateSW)
  }
  case object SlateTop extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.slateTop)
  }
  case object SlateBottom extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.slateBottom)
  }
  case object WoodLR extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.woodLR)
  }
  case object WoodDU extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.woodDU)
  }
  case object WoodNF extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.woodNF)
  }
  case object WoodLowLR extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.woodLowLR)
  }
  case object WoodLowNF extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.woodLowNF)
  }
  case object WoodHighLR extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.woodHighLR)
  }
  case object WoodHighNF extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.woodHighNF)
  }
  case object Steel extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.steel)
  }
  case object Box extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.box)
  }
  case object Sphere extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.sphere)
  }
  case object PhysicsBox extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.physicsBox)
  }
  case object PhysicsSphere extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.physicsSphere)
  }
  case object PassThrough extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.passThrough)
  }
  case object ScriptBlock extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.scriptBlock)
  }
  case object Flowers extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.flowers)
  }
  case object Particle extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.particle)
  }
  case object Marker extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.marker)
  }
  case object Shrub extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.shrub)
  }
  case object StickLR extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickLR)
  }
  case object StickDU extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickDU)
  }
  case object StickNF extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickNF)
  }
  case object StickRF extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickRF)
  }
  case object StickLF extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickLF)
  }
  case object StickLN extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickLN)
  }
  case object StickRN extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickRN)
  }
  case object StickRU extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickRU)
  }
  case object StickUF extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickUF)
  }
  case object StickLU extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickLU)
  }
  case object StickUN extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickUN)
  }
  case object StickRD extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickRD)
  }
  case object StickDF extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickDF)
  }
  case object StickLD extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickLD)
  }
  case object StickDN extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.stickDN)
  }

  case object Arch extends Tangible[Unit] {
    override val width: Int = 1
    override val height: Int = 2
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(Obj.archLower, Obj.archHigher)
  }

  case object Win extends BuiltIn[Boolean] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(Obj.win0, Obj.win1, Obj.win2, Obj.win3)

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top2(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      after,
      before
    )
  }

  case object Lose extends BuiltIn[Boolean] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(Obj.lose0, Obj.lose1, Obj.lose2, Obj.lose3)

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top2(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      after,
      before
    )
  }

  case object SetScore extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(Obj.setScore0, Obj.setScore1, Obj.setScore2, Obj.setScore3)

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top2(0)
      )

    val score: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Input,
        PinPosition.left(1)
      )

    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      score
    )
  }

  case object SetCamera extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 3
    override val ids: Vector[Obj] = Vector(
      Obj.setCamera0,
      Obj.setCamera1,
      Obj.setCamera2,
      Obj.setCamera3,
      Obj.setCamera4,
      Obj.setCamera5
    )

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top3(0)
      )

    val position: PinDefinition =
      PinDefinition(
        PinType.Vector,
        PinDirection.Input,
        PinPosition.left(2)
      )

    val rotation: PinDefinition =
      PinDefinition(
        PinType.Rotation,
        PinDirection.Input,
        PinPosition.left(1)
      )

    val distance: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Input,
        PinPosition.left(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      position,
      rotation,
      distance
    )
  }

  case object SetLight extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(Obj.setLight0, Obj.setLight1, Obj.setLight2, Obj.setLight3)

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top2(0)
      )

    val position: PinDefinition =
      PinDefinition(
        PinType.Vector,
        PinDirection.Input,
        PinPosition.left(1)
      )

    val rotation: PinDefinition =
      PinDefinition(
        PinType.Rotation,
        PinDirection.Input,
        PinPosition.left(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      position,
      rotation
    )
  }

  case object Accelerometer extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.accelerometer0,
      Obj.accelerometer1,
      Obj.accelerometer2,
      Obj.accelerometer3
    )

    val output: PinDefinition =
      PinDefinition(
        PinType.Vector,
        PinDirection.Output,
        PinPosition.right2(1)
      )

    override val pins: Vector[PinDefinition] = Vector(
      output
    )
  }

  case object ScreenSize extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.screenSize0,
      Obj.screenSize1,
      Obj.screenSize2,
      Obj.screenSize3
    )

    val screenWidth: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Output,
        PinPosition.right2(1)
      )

    val screenHeight: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Output,
        PinPosition.right2(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      screenWidth,
      screenHeight
    )
  }

  case object GetPosition extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.getPosition0,
      Obj.getPosition1,
      Obj.getPosition2,
      Obj.getPosition3
    )

    val obj: PinDefinition =
      PinDefinition(
        PinType.Object,
        PinDirection.Input,
        PinPosition.left(1)
      )
    
    val position: PinDefinition =
      PinDefinition(
        PinType.Vector,
        PinDirection.Output,
        PinPosition.right2(1)
      )

    val rotation: PinDefinition =
      PinDefinition(
        PinType.Rotation,
        PinDirection.Output,
        PinPosition.right2(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      obj,
      position,
      rotation
    )
  }

  case object SetPosition extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 3
    override val ids: Vector[Obj] = Vector(
      Obj.setPosition0,
      Obj.setPosition1,
      Obj.setPosition2,
      Obj.setPosition3,
      Obj.setPosition4,
      Obj.setPosition5
    )
    
    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )
    
    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.top3(0)
      )

    val obj: PinDefinition =
      PinDefinition(
        PinType.Object,
        PinDirection.Input,
        PinPosition.left(2)
      )

    val position: PinDefinition =
      PinDefinition(
        PinType.Vector,
        PinDirection.Input,
        PinPosition.left(1)
      )

    val rotation: PinDefinition =
      PinDefinition(
        PinType.Rotation,
        PinDirection.Input,
        PinPosition.left(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      obj,
      position,
      rotation
    )
  }

  case object Raycast extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 3
    override val ids: Vector[Obj] = Vector(
      Obj.raycast0,
      Obj.raycast1,
      Obj.raycast2,
      Obj.raycast3,
      Obj.raycast4,
      Obj.raycast5
    )

    val from: PinDefinition =
      PinDefinition(
        PinType.Vector,
        PinDirection.Input,
        PinPosition.left(2)
      )

    val to: PinDefinition =
      PinDefinition(
        PinType.Vector,
        PinDirection.Input,
        PinPosition.left(1)
      )

    val hit: PinDefinition =
      PinDefinition(
        PinType.Truth,
        PinDirection.Output,
        PinPosition.right2(2)
      )

    val hitPos: PinDefinition =
      PinDefinition(
        PinType.Vector,
        PinDirection.Output,
        PinPosition.right2(1)
      )

    val hitObj: PinDefinition =
      PinDefinition(
        PinType.Object,
        PinDirection.Output,
        PinPosition.right2(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      from,
      to,
      hit,
      hitPos,
      hitObj
    )
  }

  case object GetSize extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.getSize0,
      Obj.getSize1,
      Obj.getSize2,
      Obj.getSize3
    )

    val obj: PinDefinition =
      PinDefinition(
        PinType.Object,
        PinDirection.Input,
        PinPosition.left(1)
      )

    val min: PinDefinition =
      PinDefinition(
        PinType.Vector,
        PinDirection.Output,
        PinPosition.right2(1)
      )

    val max: PinDefinition =
      PinDefinition(
        PinType.Vector,
        PinDirection.Output,
        PinPosition.right2(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      obj,
      min,
      max
    )
  }

  case object SetVisible extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.setVisible0,
      Obj.setVisible1,
      Obj.setVisible2,
      Obj.setVisible3
    )

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top2(0)
      )

    val obj: PinDefinition =
      PinDefinition(
        PinType.Object,
        PinDirection.Input,
        PinPosition.left(1)
      )

    val visible: PinDefinition =
      PinDefinition(
        PinType.Truth,
        PinDirection.Input,
        PinPosition.left(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      obj,
      visible
    )
  }

  case object CreateObject extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.createObject0,
      Obj.createObject1,
      Obj.createObject2,
      Obj.createObject3
    )

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top2(0)
      )
    
    val original: PinDefinition =
      PinDefinition(
        PinType.Object,
        PinDirection.Input,
        PinPosition.left(1)
      )

    val copy: PinDefinition =
      PinDefinition(
        PinType.Object,
        PinDirection.Output,
        PinPosition.right2(1)
      )

    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      original,
      copy
    )
  }

  case object DestroyObject extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.destroyObject0,
      Obj.destroyObject1,
      Obj.destroyObject2,
      Obj.destroyObject3
    )

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top2(0)
      )

    val obj: PinDefinition =
      PinDefinition(
        PinType.Object,
        PinDirection.Input,
        PinPosition.left(1)
      )

    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      obj
    )
  }

  case object PlaySound extends BuiltIn[SoundOptions] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.playSound0,
      Obj.playSound1,
      Obj.playSound2,
      Obj.playSound3
    )

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top2(0)
      )

    val volume: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Input,
        PinPosition.left(1)
      )

    val pitch: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Input,
        PinPosition.left(0)
      )

    val channel: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Output,
        PinPosition.right2(1)
      )
      
    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      volume,
      pitch,
      channel
    )
  }

  case object StopSound extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.stopSound0,
      Obj.stopSound1,
      Obj.stopSound2,
      Obj.stopSound3
    )

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top2(0)
      )
    
    val channel: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.left(1)
      )
      
    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      channel
    )
  }

  case object If extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.if0,
      Obj.if1,
      Obj.if2,
      Obj.if3
    )

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top2(0)
      )

    val condition: PinDefinition =
      PinDefinition(
        PinType.Truth,
        PinDirection.Input,
        PinPosition.left(1)
      )

    val ifTrue: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.right2(1)
      )

    val ifFalse: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.right2(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      condition,
      ifTrue,
      ifFalse
    )
  }

  case object AddNumbers extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.addNumbers0,
      Obj.addNumbers1,
      Obj.addNumbers2,
      Obj.addNumbers3
    )

    val input1: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Input,
        PinPosition.left(1)
      )

    val input2: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Input,
        PinPosition.left(0)
      )

    val output: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Output,
        PinPosition.right2(1)
      )

    override val pins: Vector[PinDefinition] = Vector(
      input1,
      input2,
      output
    )
  }

  case object NumberValue extends BuiltIn[Float] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(
      Obj.number0,
      Obj.number1
    )

    val output: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Output,
        PinPosition.right2(0)
      )
    
    override val pins: Vector[PinDefinition] = Vector(
      output
    )
  }

  case object InspectNumber extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.inspectNumber0,
      Obj.inspectNumber1,
      Obj.inspectNumber2,
      Obj.inspectNumber3
    )

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.bottom(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.top2(0)
      )
    
    val value: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Input,
        PinPosition.left(1)
      )
    
    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      value
    )
  }

  case object GetNumber extends BuiltIn[String] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 1
    override val ids: Vector[Obj] = Vector(
      Obj.getNumber0,
      Obj.getNumber1
    )

    val output: PinDefinition =
      PinDefinition(
        PinType.Pointer(PinType.Number),
        PinDirection.Output,
        PinPosition.right2(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      output
    )
  }

  case object SetNumberReference extends BuiltIn[Unit] {
    override val width: Int = 2
    override val height: Int = 1
    override val depth: Int = 2
    override val ids: Vector[Obj] = Vector(
      Obj.setNumberReference0,
      Obj.setNumberReference1,
      Obj.setNumberReference2,
      Obj.setNumberReference3
    )

    val after: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Input,
        PinPosition.top2(0)
      )

    val before: PinDefinition =
      PinDefinition(
        PinType.Pull,
        PinDirection.Output,
        PinPosition.bottom(0)
      )

    val reference: PinDefinition =
      PinDefinition(
        PinType.Pointer(PinType.Number),
        PinDirection.Input,
        PinPosition.left(1)
      )

    val value: PinDefinition =
      PinDefinition(
        PinType.Number,
        PinDirection.Input,
        PinPosition.left(0)
      )

    override val pins: Vector[PinDefinition] = Vector(
      after,
      before,
      reference,
      value
    )
  }

}
