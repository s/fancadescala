package tf.bug.fancadegraph

import tf.bug.{fancadescodec => fansc}

trait Argument[T] {
  def encode(value: T)(pos: fansc.Position): Vector[fansc.Metadata]
}

object Argument {
  implicit val unitArg: Argument[Unit] = new Argument[Unit] {
    override def encode(value: Unit)(pos: fansc.Position): Vector[fansc.Metadata] = Vector.empty
  }
  implicit val stringArg: Argument[String] = new Argument[String] {
    override def encode(value: String)(pos: fansc.Position): Vector[fansc.Metadata] = Vector(
      fansc.Metadata.Text(pos, value)
    )
  }
  implicit val floatArg: Argument[Float] = new Argument[Float] {
    override def encode(value: Float)(pos: fansc.Position): Vector[fansc.Metadata] = Vector(
      fansc.Metadata.Number(pos, value)
    )
  }
  implicit val booleanArg: Argument[Boolean] = new Argument[Boolean] {
    override def encode(value: Boolean)(pos: fansc.Position): Vector[fansc.Metadata] = Vector(
      fansc.Metadata.Bool(pos, value)
    )
  }
  implicit val soundOptionsArg: Argument[SoundOptions] = new Argument[SoundOptions] {
    override def encode(value: SoundOptions)(pos: fansc.Position): Vector[fansc.Metadata] = Vector(
      fansc.Metadata.Bool(pos, value.loop),
      fansc.Metadata.PlaySoundSample(pos, value.sample)
    )
  }
}

case class SoundOptions(
  loop: Boolean,
  sample: fansc.Metadata.Sample
)
