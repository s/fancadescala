lazy val scodec = (project in file("scodec")).settings(
  organization := "tf.bug",
  name := "fancadescodec",
  version := "0.1.0",
  scalaVersion := "2.13.3",
  libraryDependencies ++= Seq(
    "org.scodec" %% "scodec-core" % "1.11.7",
    "org.scodec" %% "scodec-bits" % "1.1.17",
    "org.scodec" %% "scodec-stream" % "2.0.0",
    "org.typelevel" %% "cats-effect" % "2.1.3",
    "org.typelevel" %% "cats-core" % "2.1.1",
    "co.fs2" %% "fs2-core" % "2.4.2",
    "co.fs2" %% "fs2-io" % "2.4.2",
  ),
  mainClass in assembly := Some("tf.bug.fancadescodec.Main"),
)

lazy val graph = (project in file("graph")).settings(
  organization := "tf.bug",
  name := "fancadegraph",
  version := "0.1.0",
  scalaVersion := "2.13.3",
  resolvers += Resolver.bintrayRepo("alexknvl", "maven"),
  libraryDependencies ++= Seq(
    "org.scala-graph" %% "graph-core" % "1.13.2",
    "com.alexknvl" %% "polymorphic" % "0.5.0",
  ),
  addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full),
).dependsOn(scodec)

lazy val tagless = (project in file("tagless")).settings(
  organization := "tf.bug",
  name := "fancadetagless",
  version := "0.1.0",
  scalaVersion := "2.13.3",
  libraryDependencies ++= Seq(
    "org.scala-graph" %% "graph-core" % "1.13.2",
    "com.chuusai" %% "shapeless" % "2.3.3",
    "org.typelevel" %% "cats-core" % "2.1.1",
    "org.typelevel" %% "cats-effect" % "2.1.3",
    "org.typelevel" %% "cats-collections-core" % "0.9.0",
    "io.chrisdavenport" %% "fuuid" % "0.4.0",
    "io.higherkindness" %% "droste-core" % "0.8.0",
    "org.scalameta" %% "munit" % "0.7.9" % Test,
  ),
  testFrameworks += new TestFramework("munit.Framework"),
  addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full),
).dependsOn(scodec, graph)
